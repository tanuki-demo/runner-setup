variable "aws_access_key" {
  description = "Access Key from AWS IAM"
}

variable "aws_secret_key" {
  description = "Secret Key from AWS IAM"
}

variable "aws_region" {
  description = "AWS region."
}

variable "aws_ec2_access_key" {
  description = "Access Key with perms to create EC2 instances."
}

variable "aws_ec2_secret_key" {
  description = "Access Key with perms to create EC2 instances."
}

variable "runner_token" {
  description = "Gitlab CI token for registering a runner."
}

variable "runner_docker_image" {
  description = "Default docker image for runner."
  default     = "ubuntu:latest"
}

variable "runner_instance_type" {
  description = "Type of instance for runners."
  default     = "t2.large"
}

variable "runner_spot_price" {
  description = "Max spot price for instance."
  default     = "0.018"
}

variable "spawner_instance_type" {
  description = "Type of instance for spawner."
  default     = "t2.micro"
}

variable "runner_machine_limit" {
  description = "The maximum number of VMs to spin up to process jobs."
  default     = 6
}

# Ubuntu 16.04 LTS AMIs (HVM:ebs) will be used
data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["*hvm-ssd*-16.04*"]
  }

  owners = ["099720109477"] # Canonical
}

variable "public_key" {
  description = "SSH key to use in accessing the frontend instances."
}

variable "gitlab_host" {
  default = "https://gitlab.com"
}

variable "project" {}

variable "environment" {}

variable "vpc_cidr" {
  default = "10.8.0.0/16"
}

variable "subnet_1_cidr" {
  default = "10.8.1.0/24"
}

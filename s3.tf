resource "aws_s3_bucket" "runner-cache" {
  bucket = "${var.project}-${var.environment}-gl-runner-cache"
  acl    = "private"
}

# Specify the provider and access details
provider "aws" {
  access_key = "${var.aws_access_key}"
  secret_key = "${var.aws_secret_key}"
  region     = "${var.aws_region}"
}

#########################
#   EC2
#########################

data "template_file" "user_data" {
  template = "${file("scripts/user_data.sh")}"

  vars {
    gitlab_host          = "${var.gitlab_host}"
    runner_token         = "${var.runner_token}"
    runner_docker_image  = "${var.runner_docker_image}"
    runner_instance_type = "${var.runner_instance_type}"
    runner_ami           = "${data.aws_ami.ubuntu.id}"
    runner_vpc           = "${aws_vpc.default.id}"
    runner_subnet        = "${aws_subnet.default.id}"
    aws_key              = "${var.aws_ec2_access_key}"
    aws_secret           = "${var.aws_ec2_secret_key}"
    aws_region           = "${var.aws_region}"
    runner_machine_limit = "${var.runner_machine_limit}"
    project              = "${var.project}"
    environment          = "${var.environment}"
  }
}

resource "aws_instance" "main_runner" {
  instance_type = "${var.spawner_instance_type}"
  ami           = "${data.aws_ami.ubuntu.id}"
  key_name      = "${aws_key_pair.frontend_key.key_name}"
  subnet_id     = "${aws_subnet.default.id}"

  associate_public_ip_address = true

  user_data = "${data.template_file.user_data.rendered}"

  tags {
    Name = "${var.project}.${var.environment}.main_runner"
  }
}

resource "aws_key_pair" "frontend_key" {
  key_name   = "${var.project}-${var.environment}-runner-ssh-key"
  public_key = "${file(var.public_key)}"
}

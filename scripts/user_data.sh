#!/bin/bash

sudo apt-get update
sudo apt-get install curl
curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | sudo bash
sudo apt-get install gitlab-runner

curl -L https://github.com/docker/machine/releases/download/v0.13.0/docker-machine-`uname -s`-`uname -m` >/tmp/docker-machine &&
chmod +x /tmp/docker-machine &&
sudo cp /tmp/docker-machine /usr/local/bin/docker-machine

sudo mkdir -p /etc/gitlab-runner/certs
sudo ssh-keygen -t rsa -N "" -f /etc/gitlab-runner/certs/access.key
    
sudo gitlab-runner register --non-interactive \
    --url https://${gitlab_host}/ \
    --registration-token ${runner_token} \
    --executor "docker+machine" \
    --name "aws-gitlab-runner-spawner" \
    --limit ${runner_machine_limit} \
    --docker-image "ubuntu" \
    --docker-privileged \
    --cache-type "s3"
    --cache-s3-access-key "${aws_key}"
    --cache-s3-secret-key "${aws_secret}"
    --cache-s3-bucket-name "${project}-${environment}-gl-runner-cache"
    --cache-s3-bucket-location "${aws_region}"
    --cache-cache-shared "true"
    --machine-idle-nodes 0 \
    --machine-idle-time 2700 \
    --machine-max-builds 10 \
    --machine-machine-driver "amazonec2" \
    --machine-machine-name "auto-scale-runners-%s" \
    --machine-machine-options "amazonec2-access-key=${aws_key}" \
    --machine-machine-options "amazonec2-secret-key=${aws_secret}" \
    --machine-machine-options "amazonec2-ssh-user=ubuntu" \
    --machine-machine-options "amazonec2-region=${aws_region}" \
    --machine-machine-options "amazonec2-instance-type=${runner_instance_type}" \
    --machine-machine-options "amazonec2-ami=${runner_ami}" \
    --machine-machine-options "amazonec2-vpc-id=${runner_vpc}" \
    --machine-machine-options "amazonec2-subnet-id=${runner_subnet}" \
    --machine-machine-options "amazonec2-root-size=32" \
    --machine-machine-options "amazonec2-request-spot-instance=true" \
    --machine-machine-options "amazonec2-spot-price=${runner_spot_price}" \
    --machine-machine-options "amazonec2-ssh-keypath=/etc/gitlab-runner/certs/access.key"

sudo sed -i 's/concurrent = 1/concurrent = ${runner_machine_limit}/g' /etc/gitlab-runner/config.toml

sudo gitlab-runner start
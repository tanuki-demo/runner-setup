# Gitlab Autoscaling Runner

This module provides a Gitlab autoscaling runner deployed to AWS.

## Setup

You must first apply the ../gitlab Terraform config to create a remote
state that this project can utilize.

```
terraform init
```

## Todo

Add security group:

ssh (22/tcp)
docker (2376/tcp)
swarm (3376/tcp), only if the node is a swarm master